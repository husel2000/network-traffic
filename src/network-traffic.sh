#!/bin/bash
function usage {
	echo "";
	echo "Usage: $0 [command] ([parameter] ...)";
	echo "";
	echo "command:"
	echo "[initial] - Create the Chains, needed for Traffic-Report"
	echo "$0 initial"
	echo ""
	echo "[add] (name) (port) - Adds a new watched port for Traffic-Report"
	echo "$0 test 8080"
	echo ""
	echo "[reset] (name) - Resets the Byte-Counter"
	echo "$0 reset test"
	echo ""
	echo "[get-in-traffic] (name) - Incoming Traffic since last Reset"
	echo "$0 get-in-traffic test"
	echo ""
	echo "[get-out-traffic] (name) - Outgoing Traffic since last Reset"
	echo "$0 get-out-traffic test"

}

#Checks...
if [[ `which iptables` == "" ]] ; then
	echo "iptables is missing? trying sudo..."
	sudo $0 $@
	exit;
fi

if [ $# -lt 1 ] ; then
	usage
	exit;
fi

case "$1" in 

	help)
		usage
		;;
	initial)
		#Allgemeine Kette anlegen
		iptables -N traffic_input
		iptables -N traffic_output
		#Traffic durch Kette leiten
		iptables -I INPUT -j traffic_input
		iptables -I OUTPUT -j traffic_output
		;;
	add)
		if [ $# -ne 3 ] ; then 
			echo "Wrong Usage..." && usage && exit
		fi
		NAME=$2
		PORT=$3
		echo "Adding \"$NAME\" for TCP-Port $PORT"

		iptables -N traffic_output_$NAME
		iptables -N traffic_input_$NAME

		iptables -I traffic_input -p tcp --dport $PORT -j traffic_input_$NAME
		iptables -I traffic_output -p tcp --dport $PORT -j traffic_output_$NAME
		
		$0 reset $2
		;;
	reset)
		if [ $# -ne 2 ] ; then
			echo "Wrong Usage..." && usage && exit
		fi
		echo "Reset $2"
		iptables -D  traffic_input_$2 1 2>/dev/null
		iptables -D  traffic_output_$2 1 2>/dev/null
		iptables -I traffic_input_$2 -m comment --comment "`date +%s`" -j RETURN
		iptables -I traffic_output_$2 -m comment --comment "`date +%s`" -j RETURN

		;;

	get-in-traffic)
		if [ $# -ne 2 ] ; then
			echo "Wrong Usage..." && usage && exit
		fi
		iptables -nvxL traffic_input_$2 | tail -n 1 |awk '{ print $2}'
		;;
	get-out-traffic)
		if [ $# -ne 2 ] ; then
			echo "Wrong Usage..." && usage && exit
		fi
		iptables -nvxL traffic_output_$2 | tail -n 1 |awk '{ print $2}'
		;;
	get-in-bps)
		if [ $# -ne 2 ] ; then
			echo "Wrong Usage..." && usage && exit
		fi
		START=$(iptables -nvxL traffic_input_$2 | tail -n 1| awk {' print $11'})
		END=$(date +%s)
		TRAFFIC=$($0 get-in-traffic $2)
		echo $TRAFFIC / \($END-$START\) | bc
		;;
	get-out-bps)
		if [ $# -ne 2 ] ; then
			echo "Wrong Usage..." && usage && exit
		fi
		START=$(iptables -nvxL traffic_input_$2 | tail -n 1| awk {' print $11'})
		END=$(date +%s)
		TRAFFIC=$($0 get-out-traffic $2)
		echo $TRAFFIC / \($END - $START\) |bc
		;;
esac

exit



#Zurücksetzen einer Kette
iptables -Z traffic_bitcoin

iptables -nvxL traffic_bitcoin | grep n |awk '{ print $2}'
